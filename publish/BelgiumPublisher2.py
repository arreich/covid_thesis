# To add a new cell, type '  '
# To add a new markdown cell, type '   [markdown]'

  
import datetime as dt
from datetime import datetime
from datetime import timedelta
import requests
import urllib, json, sys
from arcgis.gis import GIS
from arcgis.gis import Layer
from arcgis.features import FeatureLayerCollection
from arcgis.features import Table
from arcgis.features import FeatureLayer
from credentials import *
import os
from glob import glob


  
## Returns the state_poly and state_centroid layers
def getLayers(gis, itemid, indices):
    print("    Getting Layers from portal...")
    item = gis.content.get(itemid)
    poly = FeatureLayer.fromitem(item, indices[0]) 
    centroids = FeatureLayer.fromitem(item, indices[1]) 
    return [poly, centroids]


  
#Returns the table by tableid and index
def getTable(gis, tableid, index):
    print("    Getting Rolling Table...")
    item = gis.content.get(tableid)
    table = Table.fromitem(item, index)
    return table


  
def connectGIS(username, password):
    print("Connecting to Portal...")
    gis = GIS("https://v0yager.maps.arcgis.com/home/", username= username, password = password)
    return gis


  
def setStateDict(data, ref, date):
    stateDict = dict() 
    for f in data['features']:
        props = f["properties"]
        if not props[ref['StateName']] in stateDict.keys():
            stateDict[props[ref['StateName']]] = {'Total_New': 0, 'Daily_New': 0, 'Total_Hosp': 0, 'Daily_Hosp': 0, 
                                                 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Date': date, 'Total_Recovered': 0, 'Daily_Recovered': 0}
    return stateDict
  
def setCountryDict(date):
    countryDict = {'Total_New': 0, 'Daily_New': 0, 'Total_Hosp': 0, 'Daily_Hosp': 0, 
                                                 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Date': date, 'Total_Recovered': 0, 'Daily_Recovered': 0 }
    return countryDict

#Convert unix time stamp to a datetime
def convertUnixTime(date):
    ts = int(date)
    format_date = dt.datetime.utcfromtimestamp(int(ts/1000))
    d = format_date.strftime('%m/%d/%Y, %H:%M:%S')
    return d.split(',')[0]
  
def processDataFile(caseJSON, mortJSON, hospJSON, ref, date):
    print("    Processing JSON file...")
    with open(caseJSON) as casefile:
        r_data = casefile.read()
    case = json.loads(r_data)
    with open(mortJSON) as mortfile:
        r_data = mortfile.read()
    mort = json.loads(r_data)
    with open(hospJSON) as hospfile:
        r_data = hospfile.read()
    hosp = json.loads(r_data)

    states = setStateDict(case, ref, date) 
    country = setCountryDict(date)
   
    for f in case["features"]:
        props = f["properties"]
        if 'Total_New' in ref.keys():
            states[props[ref['StateName']]]["Total_New"] += props[ref['Total_New']]
            country['Total_New'] += props[ref['Total_New']]
        else:
            states[props[ref['StateName']]]["Total_New"] = 0
        if dt.datetime.strptime(props[ref['Date']], "%Y-%m-%d") == states[props[ref['StateName']]]['Date']:
            states[props[ref['StateName']]]['Daily_New'] += props[ref['Total_New']]
            country['Daily_New'] += props[ref['Total_New']]
    for f in mort["features"]:
        props = f["properties"]
        if 'Total_Deaths' in ref.keys():
            states[props[ref['StateName']]]["Total_Deaths"] += props[ref['Total_Deaths']]
            country['Total_Deaths'] += props[ref['Total_Deaths']]
        else:
            states[props[ref['StateName']]]["Total_Deaths"] = 0
        if dt.datetime.strptime(props[ref['Date']], "%Y-%m-%d") == states[props[ref['StateName']]]['Date']:
            states[props[ref['StateName']]]['Daily_Deaths'] += props[ref['Total_Deaths']]
            country['Daily_Deaths'] += props[ref['Total_Deaths']]
    for f in hosp["features"]:
        props = f["properties"]
        if 'Total_Hosp' in ref.keys():
            states[props[ref['StateName']]]["Total_Hosp"] += props[ref['Total_Hosp']]
            country['Total_Hosp'] += props[ref['Total_Hosp']]
        else: 
            states[props[ref['StateName']]]["Total_Hosp"] = 0
        if dt.datetime.strptime(props[ref['Date']], "%Y-%m-%d") == states[props[ref['StateName']]]['Date']:
            states[props[ref['StateName']]]['Daily_Hosp'] += props[ref['Total_Hosp']]
            country['Daily_Hosp'] += props[ref['Total_Hosp']]
        if 'Daily_Recovered' in ref.keys():
            states[props[ref['StateName']]]["Total_Recovered"] += props[ref['Daily_Recovered']]
            country['Total_Recovered'] += props[ref['Daily_Recovered']]
        else:
            states[props[ref['StateName']]]["Total_Recovered"] = "None"
            country['Total_Recovered'] = "None"
        if dt.datetime.strptime(props[ref['Date']], "%Y-%m-%d") == states[props[ref['StateName']]]['Date']:
            states[props[ref['StateName']]]['Daily_Recovered'] += props[ref['Daily_Recovered']]
            country['Daily_Recovered'] += props[ref['Daily_Recovered']]
    return [states, country]


  
def updateStateLayers(stateDict, gis, itemid, ref, tableid, index):
    print("    Updating layers on portal...")
    layers = getLayers(gis, itemid, [2,0])
    layer_mod_date = layers[0].query(where=f"NUTS0 = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = stateDict[list(stateDict.keys())[0]]['Date']
    if ( file_mod_date > layer_mod_date ): 
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")  
 
        for layer in layers:
            print(layer)
            for r in stateDict.keys():
                #print(r)
                where = "NUTS0 = '{0}' AND NAME = '{1}'".format(ref['NUTS0'],r)
                success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(stateDict[r]["Total_New"])},
                                                                        {"field": "Daily_New", "value": int(stateDict[r]["Daily_New"])}, 
                                                                        {"field": "Total_Deaths", "value": int(stateDict[r]["Total_Deaths"])},
                                                                        {"field": "Daily_Deaths", "value": int(stateDict[r]["Daily_Deaths"])},
                                                                        {"field": "Total_Hosp", "value": int(stateDict[r]["Total_Hosp"])},
                                                                        {"field": "Daily_Hosp", "value": int(stateDict[r]["Daily_Hosp"])},
                                                                        {"field": "Total_Recovered", "value": int(stateDict[r]["Total_Recovered"])},
                                                                        {"field": "Daily_Recovered", "value": int(stateDict[r]["Daily_Recovered"])},
                                                                    {"field": "Date", "value": stateDict[r]['Date']}])
        where = "NUTS0 = '{0}'".format(ref['NUTS0'])
        state_centroids = layers[1].query(where=where, return_geometry=False)
        
        table = getTable(gis, tableid, index)
        print("Updating Rolling State table...")
        table.edit_features(adds = state_centroids)
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
      


  
def updateCountryLayers(country, gis, itemid, ref, tableid, index):
    #Calculates fields of state and state_centroids to update their figures. 
    #Calculates Daily values by querying old values and finding differences
    print("    Updating Country layers on portal...")
    layers = getLayers(gis, itemid, [3,1])
    layer_mod_date = layers[0].query(where=f"NUTS = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = country['Date']
    if ( file_mod_date > layer_mod_date ):
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")
                
        for layer in layers:
            print(layer)
            where = "NUTS = '{0}'".format(ref['NUTS0'])
            #Do the field calculations here    
            success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(country["Total_New"])},
                                                                    {"field": "Daily_New", "value": int(country["Daily_New"])}, 
                                                                    {"field": "Total_Deaths", "value": int(country["Total_Deaths"])},
                                                                    {"field": "Daily_Deaths", "value": int(country["Daily_Deaths"])},
                                                                    {"field": "Total_Hosp", "value": int(country["Total_Hosp"])}, 
                                                                    {"field": "Daily_Hosp", "value": int(country["Daily_Hosp"])},
                                                                    {"field": "Total_Recovered", "value": int(country["Total_Recovered"])}, 
                                                                    {"field": "Daily_Recovered", "value": int(country["Daily_Recovered"])},
                                                                    {"field": "Date", "value": country['Date']}])
        #Update the rolling COUNTRY table by querying country_centroids for the NUTS0 
        where = "NUTS = '{0}'".format(ref['NUTS0'])
        country_centroids = layers[1].query(where=where, return_geometry=False)

        table = getTable(gis, tableid, index)
        print("Updating Rolling Country table...")
        table.edit_features(adds = country_centroids)  
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
        

  
def job( ):
    global username
    global password
    
    scratchFS = '2a0326b454174cada1c30d397316717a'
    scratchTB = 'a0d0e429406f4063a1ce541e0a60ffdb'
    
    NutsFS = 'f47c01c969b14fc69980b3211b6ca542'
    NutsRollingState = '7a4e41403c1144a99cf5462bdb857756'
    NutsRollingCountry = 'f115f83f630f4fbe8eeb4d6124853c3d'
    

    gis = connectGIS(username, password)
    
    
    countries = {'Belgium': {}}
    c_ref = {'Belgium':{'type':'Rolling','StateName':'REGION', 'NUTS0':'BE', 'Total_New':'CASES', 'Total_Deaths': 'DEATHS', 'Total_Hosp': 'TOTAL_IN', 'Date': 'DATE', 
                'Daily_Hosp':'NEW_IN', 'Daily_Recovered': 'NEW_OUT'}}
    
    for c in countries: 
        # Usually we will only process data from the most recent file:
        # file_list must remain a list so be sure to keep the outer [ ]
        # Dates in State and Country tables must be manually back-dated
        # to re-process data that already exists in the tables.  You will 
        # probably want to delete rows from Rolling_State and Rolling_Country_3day
        # also to prevent duplicate entries

        casepath = os.path.join(os.path.dirname(__file__), '../json/', c , 'Cases', c + "_Cases")
        case_list = glob(f"{casepath}202?????.json")
        case_list.sort()
        # Comment out the following line to attempt to process all data files
        #case_list = [case_list[-1]]

        mortpath = os.path.join(os.path.dirname(__file__), '../json/', c , 'Mort', c + "_MORT")
        mort_list = glob(f"{mortpath}202?????.json")
        mort_list.sort()
        # Comment out the following line to attempt to process all data files
        #mort_list = [mort_list[-1]]

        hosppath = os.path.join(os.path.dirname(__file__), '../json/', c , 'Hosp', c + "_HOSP")
        hosp_list = glob(f"{hosppath}202?????.json")
        hosp_list.sort()
        # Comment out the following line to attempt to process all data files
        #hosp_list = [hosp_list[-1]]
        
        for case, mort, hosp in zip(case_list, mort_list, hosp_list):
            date = dt.datetime.strptime(case[-13:-5], "%Y%m%d") - dt.timedelta(days=2)
            date = dt.datetime.strftime(date, "%m/%d/%Y") + ", 00:00:00"
            
            [states, country] = processDataFile(case, mort, hosp, c_ref[c], date)
            countries[c] = country

            print(" # UPDATING SCRATCH #")
            updateStateLayers(states, gis, scratchFS, c_ref[c], scratchTB, index=2)
            updateCountryLayers(country, gis, scratchFS, c_ref[c], scratchTB, index=1)

            production=False
            if production:
                print(" # UPDATING PRODUCTION #")
                updateStateLayers(states, gis, NutsFS, c_ref[c], NutsRollingState, index=0)
                updateCountryLayers(country, gis, NutsFS, c_ref[c], NutsRollingCountry, index=0)

            print("## Process Complete for {0} data from {1}, {2}, {3} ##\n".format(c, case.split('\\')[-1], hosp.split('\\')[-1], mort.split('\\')[-1]  ))
        
            #print(countries)
    

job()

