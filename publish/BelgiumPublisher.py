# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
import json
import datetime as dt
from datetime import datetime
from datetime import timedelta
import requests
import urllib, json, sys, collections
import csv
import arcgis
from arcgis.gis import GIS
from arcgis.gis import Layer
from arcgis.features import FeatureLayerCollection
from arcgis.features import Table
from arcgis.features import FeatureLayer

from getpass import getpass

from credentials import *


# %%
## Returns the state_poly and state_centroid layers
def getLayers(gis, itemid, indices):
    print("Getting Layers from portal...")
    item = gis.content.get(itemid)
    poly = FeatureLayer.fromitem(item, indices[0]) 
    centroids = FeatureLayer.fromitem(item, indices[1]) 
    return [poly, centroids]
   


# %%
#Returns the table by tableid and index
def getTable(gis, tableid, index):
    print("Getting Rolling Table...")
    item = gis.content.get(tableid)
    table = Table.fromitem(item, index)
    return table



# %%
#Connection to 60th GPC portal
def connectGIS(username, password):
    print("Connecting to Portal...")
    gis = GIS("https://v0yager.maps.arcgis.com/home/", username= username, password = password)
    return gis


# %%
def processCaseFile(caseJSON, date):
    print("Processing Belgium Case File...")
    with open(caseJSON) as casefile:
        r_case = casefile.read()
        cases = json.loads(r_case)
        
        #list of Brussels features
        blist = []
        b_dailynew = 0
        bcases = 0
        #list of Flanders features
        flist = []
        f_dailynew = 0
        fcases = 0
        #list of Wallonia features
        wlist = []
        w_dailynew = 0
        wcases = 0
        #Add up total cases by region and cases on a given date by region
        for f in cases["features"]:
            props = f["properties"]
            if props["REGION"] == "Brussels":
                blist.append(f)
                bcases += props["CASES"]
                if props["DATE"] == date:
                    b_dailynew += props["CASES"]
            elif props["REGION"] == "Wallonia":
                wlist.append(f)
                wcases += props["CASES"]
                if props["DATE"] == date:
                    w_dailynew += props["CASES"]
            elif props["REGION"] == "Flanders":
                flist.append(f)
                fcases += props["CASES"]
                if props["DATE"] == date:
                    f_dailynew += props["CASES"]
        #Return dictionary of total and daily new cases by region
        return {"Brussels": {"total_new": bcases,"daily_new": b_dailynew}, 
                "Wallonia": {"total_new": wcases,"daily_new": w_dailynew}, 
                "Flanders": {"total_new": fcases,"daily_new": f_dailynew}
               }


# %%
def processHospFile(hospJSON, date):
    print("Processing Belgium Hospitalization file...")
    with open(hospJSON) as hospfile:
        r_hosp = hospfile.read()
        hosp = json.loads(r_hosp)
        
        #list of Brussels features
        blist = []
        bhosp_total = 0
        bhosp_new = 0
        bhosp_out = 0
        brecover_total = 0
        #list of Flanders features
        flist = []
        fhosp_total = 0
        fhosp_new = 0
        fhosp_out = 0
        frecover_total = 0
        #list of Wallonia features
        wlist = []
        whosp_total = 0
        whosp_new = 0
        whosp_out = 0
        wrecover_total = 0
        #Add up hospitalizations by region and by a given date by region
        for f in hosp["features"]:
            props = f["properties"]
            if props["REGION"] == "Brussels":
                blist.append(f)
                bhosp_total += props["TOTAL_IN"]
                brecover_total += props["NEW_OUT"]
                if props["DATE"] == date:
                    bhosp_new += props["NEW_IN"]
                    bhosp_out += props["NEW_OUT"]
            elif props["REGION"] == "Wallonia":
                wlist.append(f)
                whosp_total += props["TOTAL_IN"]
                wrecover_total += props["NEW_OUT"]
                if props["DATE"] == date:
                    whosp_new += props["NEW_IN"]
                    whosp_out += props["NEW_OUT"]
            elif props["REGION"] == "Flanders":
                flist.append(f)
                fhosp_total += props["TOTAL_IN"]
                frecover_total += props["NEW_OUT"]
                if props["DATE"] == date:
                    fhosp_new += props["NEW_IN"]
                    fhosp_out += props["NEW_OUT"]
        #Return dictionary of total and daily hospitalizations for each region
        return {"Brussels": {"total_hosp": bhosp_total, "total_recover": brecover_total, "daily_hosp": bhosp_new, "daily_recover": bhosp_out},
                "Wallonia": {"total_hosp": whosp_total, "total_recover": wrecover_total,"daily_hosp": whosp_new, "daily_recover": whosp_out}, 
                "Flanders": {"total_hosp": fhosp_total, "total_recover": frecover_total,"daily_hosp": fhosp_new, "daily_recover": fhosp_out}}


# %%
def processMortFile(mortJSON, date):
    print("Processing Belgium deaths file...")
    with open(mortJSON) as mortfile:
        r_mort = mortfile.read()
        morts = json.loads(r_mort)
        #list of Brussels features
        blist = []
        bdeaths = 0
        b_dailydeaths = 0
        #list of Flanders features
        flist = []
        fdeaths = 0
        f_dailydeaths = 0
        #list of Wallonia features
        wlist = []
        wdeaths = 0
        w_dailydeaths = 0
        
        #Add up all deaths for each region and add up all deaths on the given date for each region
        for f in morts["features"]:
            props = f["properties"]
            if props["REGION"] == "Brussels":
                blist.append(f)
                bdeaths+= props["DEATHS"]
                if props["DATE"] == date:
                    b_dailydeaths += props["DEATHS"]
            elif props["REGION"] == "Wallonia":
                wlist.append(f)
                wdeaths += props["DEATHS"]
                if props["DATE"] == date:
                    b_dailydeaths += props["DEATHS"]
            elif props["REGION"] == "Flanders":
                flist.append(f)
                fdeaths += props["DEATHS"]
                if props["DATE"] == date:
                    b_dailydeaths += props["DEATHS"]
        #Return dictionary of totals and daily deaths for each region
        return {"Brussels": {"total_deaths": bdeaths, "daily_deaths": b_dailydeaths}, 
                "Wallonia": {"total_deaths": wdeaths, "daily_deaths": w_dailydeaths}, 
                "Flanders": {"total_deaths": fdeaths, "daily_deaths": f_dailydeaths}}


# %%
def main(gis, caseJSON, hospJSON, mortJSON, date, outfile, itemid, tableid):
    #Process each JSON file according to its format
    
    
    #Write output to CSV (optional)
    """with open(outfile, 'w') as output:
        out = csv.writer(output, delimiter=' ',lineterminator='\r')
        out.writerow("Region,Date,Total_New,Daily_New,Total_Hosp,Daily_Hosp,Total_Deaths,Daily_Deaths")
        out.writerow("Brussels,{0},{1},{2},{3},{4},{5},{6}".format(date, case_updates["Brussels"]["total_new"],
                                                 case_updates["Brussels"]["daily_new"],
                                                 hosp_updates["Brussels"]["total_hosp"],
                                                 hosp_updates["Brussels"]["daily_hosp"],
                                                 mort_updates["Brussels"]["total_deaths"],
                                                 mort_updates["Brussels"]["daily_deaths"] ))
        
        out.writerow("Flanders,{0},{1},{2},{3},{4},{5},{6}".format(date, case_updates["Flanders"]["total_new"],
                                                 case_updates["Flanders"]["daily_new"],
                                                 hosp_updates["Flanders"]["total_hosp"],
                                                 hosp_updates["Flanders"]["daily_hosp"],
                                                 mort_updates["Flanders"]["total_deaths"],
                                                 mort_updates["Flanders"]["daily_deaths"] ))
        
        out.writerow("Wallonia,{0},{1},{2},{3},{4},{5},{6}".format(date, case_updates["Wallonia"]["total_new"],
                                                 case_updates["Wallonia"]["daily_new"],
                                                 hosp_updates["Wallonia"]["total_hosp"],
                                                 hosp_updates["Wallonia"]["daily_hosp"],
                                                 mort_updates["Wallonia"]["total_deaths"],
                                                 mort_updates["Wallonia"]["daily_deaths"] ))"""
    
    #printUpdates(case_updates, hosp_updates, mort_updates)
    
    #Calculate fields into the output dataset
    
    
    


# %%
def updateStateLayers(case_updates, mort_updates, hosp_updates, gis, itemid, tableid, date):
    layers = getLayers(gis, itemid, [3,2])
    for layer in layers:
        print("Updating layer: ")
        print(layer)
        for r in ["Brussels", "Flanders", "Wallonia"]:
            #print(r)
            where = "NUTS0 = 'BE' AND NAME = '{0}'".format(r)
            #print(layer.validate_sql(where, sql_type='where'))
            
            success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(case_updates[r]["total_new"])},
                                                                    {"field": "Daily_New", "value": int(case_updates[r]["daily_new"])}, 
                                                                    {"field": "Total_Deaths", "value": int(mort_updates[r]["total_deaths"])},
                                                                    {"field": "Daily_Deaths", "value": int(mort_updates[r]["daily_deaths"])},
                                                                   {"field": "Daily_Recovered", "value": int(hosp_updates[r]["daily_recover"])},
                                                                    {"field": "Total_Hosp", "value": int(hosp_updates[r]["total_hosp"])},
                                                                    {"field": "Daily_Hosp", "value": int(hosp_updates[r]["daily_hosp"])},
                                                                    {"field": "Total_Recovered", "value": int(hosp_updates[r]["total_recover"])},
                                                                   {"field": "Date", "value": date}])                                                    
    #Update rolling state table    
    print("Appending Rolling State table...")
    where = "NUTS0 = '{0}'".format('BE')
    state_centroids = layers[1].query(where=where, return_geometry=False)
    scratch = 2
    live = 0
    table = getTable(gis, tableid, scratch)
    print("Updating Rolling State table...")
    table.edit_features(adds = state_centroids)


# %%
def setCountryDict():
    countryDict = {'Total_New':0, 'Daily_New':0, 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Total_Hosp': 0, 'Daily_Hosp':0, 'Total_Recovered': 0, 'Daily_Recovered': 0, 'Date': ''}
    return countryDict


# %%
def updateCountryLayers(case_updates, mort_updates, hosp_updates, gis, itemid, tableid, date):
    layers = getLayers(gis, itemid, [1,0])
    for layer in layers:
        print("Updating layer: ")
        print(layer)
        country = setCountryDict()
        for r in ["Brussels", "Flanders", "Wallonia"]:
            country['Total_New'] += int(case_updates[r]["total_new"])
            country['Daily_New'] += int(case_updates[r]["daily_new"])
            country['Total_Deaths'] += int(mort_updates[r]["total_deaths"])
            country['Daily_Deaths'] +=  int(mort_updates[r]["daily_deaths"])
            country['Total_Recovered'] += int(hosp_updates[r]["total_recover"])
            country['Daily_Recovered'] += int(hosp_updates[r]["daily_recover"])
            country['Total_Hosp'] +=  int(hosp_updates[r]["total_hosp"])
            country['Daily_Hosp'] += int(hosp_updates[r]["daily_hosp"])
            country['Date'] = date
             #print(r)
            where = "NUTS = 'BE'"
            #print(layer.validate_sql(where, sql_type='where'))
            
            success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": country['Total_New']},
                                                                    {"field": "Daily_New", "value": country['Daily_New']}, 
                                                                    {"field": "Total_Deaths", "value": country['Total_Deaths']},
                                                                    {"field": "Daily_Deaths", "value": country['Daily_Deaths']},
                                                                   {"field": "Daily_Recovered", "value": country['Daily_Recovered']},
                                                                    {"field": "Total_Hosp", "value": country['Total_Hosp']},
                                                                    {"field": "Daily_Hosp", "value": country['Daily_Hosp']},
                                                                    {"field": "Total_Recovered", "value": country['Total_Recovered']},
                                                                   {"field": "Date", "value": date}])                                                    
    #Update rolling state table    
    print("Appending Rolling Country table...")
    where = "NUTS = '{0}'".format('BE')
    country_centroids = layers[1].query(where=where, return_geometry=False)
    scratch = 1
    live = 0
    table = getTable(gis, tableid, 1)
    print("Updating Rolling Country table...")
    table.edit_features(adds = country_centroids)


# %%
#Optional method to print directly in Jupyter, useful for testing
def printUpdates(case_updates, hosp_updates, mort_updates):  
    print("Brussels:  \n")
    print("Total Cases: %d  " % case_updates["Brussels"]["total_new"])
    print("Daily New Cases: %d  " % case_updates["Brussels"]["daily_new"])
    print("Total Hospitalizations: %d  " % hosp_updates["Brussels"]["total_hosp"])
    print("Daily New Hospitalizations: %d  " % hosp_updates["Brussels"]["daily_hosp"])
    print("Total Deaths: %d  " % mort_updates["Brussels"]["total_deaths"])
    print("Daily Deaths: %d   " % mort_updates["Brussels"]["daily_deaths"])
    print()
    print("Flanders:  \n")
    print("Total Cases: %d  " % case_updates["Flanders"]["total_new"])
    print("Daily New Cases: %d  " % case_updates["Flanders"]["daily_new"])
    print("Total Hospitalizations: %d  " % hosp_updates["Flanders"]["total_hosp"])
    print("Daily New Hospitalizations: %d  " % hosp_updates["Flanders"]["daily_hosp"])
    print("Total Deaths: %d  " % mort_updates["Flanders"]["total_deaths"])
    print("Daily Deaths: %d   " % mort_updates["Flanders"]["daily_deaths"])
    print()
    print("Wallonia:  \n")
    print("Total Cases: %d  " % case_updates["Wallonia"]["total_new"])
    print("Daily New Cases: %d  " % case_updates["Wallonia"]["daily_new"])
    print("Total Hospitalizations: %d  " % hosp_updates["Wallonia"]["total_hosp"])
    print("Daily New Hospitalizations: %d  " % hosp_updates["Wallonia"]["daily_hosp"])
    print("Total Deaths: %d  " % mort_updates["Wallonia"]["total_deaths"])
    print("Daily Deaths: %d   " % mort_updates["Wallonia"]["daily_deaths"])
   


# %%
def job():

    #Choose a time delta
    #date is the date of the data
    #fdate is the date of the file (one day ahead of the data)
    #The data is published the day after to have the complete numbers for the previous day. 
    r = 3
    date = dt.datetime.strftime(dt.datetime.today() - timedelta(days=r), "%Y-%m-%d")
    fdate = dt.datetime.strftime(dt.datetime.today() - timedelta(days=r-1), "%Y%m%d")
    #Filepaths to the Dropbox JSON backups
    case = os.path.join('../json/Belgium/Cases/Belgium_Cases', fdate + ".json")
    hosp = os.path.join('../json/Belgium/Hosp/Belgium_HOSP', fdate + ".json")
    mort = os.path.join('../json/Belgium/Mort/Belgium_MORT', fdate + ".json")
    #Optional CSV output
    
    scratchFS = '2a0326b454174cada1c30d397316717a'
    scratchTB = 'a0d0e429406f4063a1ce541e0a60ffdb'
    
    NutsFS = 'f47c01c969b14fc69980b3211b6ca542'
    NutsRollingState = '7a4e41403c1144a99cf5462bdb857756'
    NutsRollingCountry = 'f115f83f630f4fbe8eeb4d6124853c3d'
    
    global username
    global password

    try:
        gis = connectGIS(username, password)
    except:
        #Another try at logging in
        try:
            username = input("Username: ")
            password = getpass("Password: ")
            gis = connectGIS(username, password)
        except:
            print("Login error! Run again.")
    #Run the main() method
    case_updates = processCaseFile(case, date)
    hosp_updates = processHospFile(hosp, date)
    mort_updates = processMortFile(mort, date)
    #Displays date of the data 
    print(date)
    #Change date format
    date = dt.datetime.strptime(date,"%Y-%m-%d")
    date = dt.datetime.strftime(date, "%m/%d/%Y")
    updateStateLayers(case_updates, mort_updates, hosp_updates, gis, scratchFS, scratchTB, date)
    updateCountryLayers(case_updates, mort_updates, hosp_updates, gis, scratchFS, scratchTB, date)
    print("Done with Belgium!")


# %%
"""schedule.every(1).day.at("02:30").do(job)
while True:
    schedule.run_pending()
    time.sleep(60)"""

job()

