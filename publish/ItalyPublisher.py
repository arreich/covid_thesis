# To add a new cell, type ''
# To add a new markdown cell, type ' [markdown]'


import json
import datetime as dt
from datetime import datetime
from datetime import timedelta
import urllib, sys
from arcgis.gis import GIS
from arcgis.gis import Layer
from arcgis.features import FeatureLayerCollection
from arcgis.features import Table
from arcgis.features import FeatureLayer
from credentials import *
import os
from glob import glob



## Returns the state_poly and state_centroid layers
def getLayers(gis, itemid, indices):
    print("    Getting Layers from portal...")
    item = gis.content.get(itemid)
    poly = FeatureLayer.fromitem(item, indices[0]) 
    centroids = FeatureLayer.fromitem(item, indices[1]) 
    return [poly, centroids]



#Returns the table by tableid and index
def getTable(gis, tableid, index):
    print("    Getting Rolling Table...")
    item = gis.content.get(tableid)
    table = Table.fromitem(item, index)
    return table



def connectGIS(username, password):
    # print("Connecting to Portal...")
    gis = GIS("https://v0yager.maps.arcgis.com/home/", username= username, password = password)
    return gis




#This one sets the states explicitly because the Kosovo data is taken from Serbia's data, so we don't need all of Serbia. 

def setStateDict(data, ref):
    stateDict = dict() 
    for f in data["features"]:
        props = f['properties']
            
        if not props[ref['StateName']] in stateDict.keys():
            stateDict[props[ref['StateName']]] = {'Total_New': 0, 'Daily_New': 0, 'Total_Recovered': 0, 'Daily_Recovered': 0, 
                                                 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Date': '', 'Total_Hosp': 0, 'Daily_Hosp': 0}
    return stateDict



def setCountryDict():
    countryDict = {'Total_New': 0, 'Daily_New': 0, 'Total_Recovered': 0, 'Daily_Recovered': 0, 
                                                 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Date': '', 'Total_Hosp': 0, 'Daily_Hosp': 0}
    return countryDict

#Convert unix time stamp to a datetime
def convertUnixTime(date):
    ts = int(date)
    format_date = dt.datetime.utcfromtimestamp(int(ts/1000))
    return format_date #.strftime('%m/%d/%Y, %H:%M:%S')



def processDataFile(dataJSON, ref):
    print("    Processing JSON file...")
    with open(dataJSON) as datafile:
        r_data = datafile.read()
    data = json.loads(r_data)
    
    states = setStateDict(data, ref) 
    country = setCountryDict()

    c_ref = {'Italy':{'type':'Editing','StateName':'denominazione_regione', 'NUTS0':'IT', 'Total_New':'totale_casi', 'Daily_New': 'nuovi_positivi',
                    'Total_Hosp':'totale_ospedalizzati', 'Total_Deaths': 'deceduti', 'Total_Recovered': 'dimessi_guariti', 'Date': 'data'}}
    
   
    for f in data["features"]:
        props = f["properties"]
        if 'Total_Hosp' in ref.keys():
            states[props[ref['StateName']]]["Total_Hosp"] += props[ref['Total_Hosp']]
            country['Total_Hosp'] += props[ref['Total_Hosp']]
        else:
            states[props[ref['StateName']]]["Total_Hosp"] += 0
            country['Total_Hosp'] += 0
        if 'Total_Deaths' in ref.keys():
            states[props[ref['StateName']]]["Total_Deaths"] += props[ref['Total_Deaths']]
            country['Total_Deaths'] += props[ref['Total_Deaths']]
        else:
            states[props[ref['StateName']]]["Total_Deaths"] = 0
            country['Total_Deaths'] = 0
        if 'Total_Recovered' in ref.keys():
            states[props[ref['StateName']]]['Total_Recovered'] += props[ref['Total_Recovered']]
            country['Total_Recovered'] += props[ref['Total_Recovered']]
        else:
            states[props[ref['StateName']]]['Total_Recovered'] = "None"
            country['Total_Recovered'] = "None"

        if 'Date' in ref.keys():
            states[props[ref['StateName']]]["Date"] = props[ref['Date']]
            country['Date'] = props[ref['Date']]
        if 'Daily_Deaths' in ref.keys():
            states[props[ref['StateName']]]["Daily_Deaths"] += props[ref['Daily_Deaths']]
            country['Daily_Deaths'] += props[ref['Daily_Deaths']]
        else:
            states[props[ref['StateName']]]["Daily_Deaths"] = "None"
            country['Daily_Deaths'] = "None"
        if 'Daily_Hosp' in ref.keys():
            states[props[ref['StateName']]]["Daily_Hosp"] += props[ref['Daily_Hosp']]
            country['Daily_Hosp'] += props[ref['Daily_Hosp']]
        else:
            states[props[ref['StateName']]]["Daily_Hosp"] = "None"
            country['Daily_Hosp'] = "None"
        if 'Daily_Recovered' in ref.keys():
            states[props[ref['StateName']]]["Daily_Recovered"] += props[ref['Daily_Recovered']]
            country['Daily_Recovered'] += props[ref['Daily_Recovered']]
        else:
            states[props[ref['StateName']]]["Daily_Recovered"] = "None"
            country['Daily_Recovered'] = "None"
    return [states, country]



def updateStateLayers(stateDict, gis, itemid, ref, tableid, index):
    print("    Updating State layers on portal...")
    layers = getLayers(gis, itemid, [2,0])

    # Get last update in epoch format
    layer_mod_date = layers[0].query(where=f"NUTS0 = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = dt.datetime.strptime(stateDict[list(stateDict.keys())[0]]['Date'], "%Y-%m-%dT%H:%M:%S")
    if ( file_mod_date > layer_mod_date ):  
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")  

        for layer in layers:
            # print(layer)
            for r in stateDict.keys():
                #print(r)
                state = r.replace('\'', '')
                state = state.replace('-', ' ')
                where = "NUTS0 = '{0}' AND NAME = '{1}'".format(ref['NUTS0'],state)            
                if stateDict[r]["Daily_Hosp"] == "None":          
                    old_hosp = layer.query(where=where, out_fields=['Total_Hosp'], return_geometry=False)
                    old_hosp = old_hosp.value['features'][0]['attributes']['Total_Hosp']
                    if(old_hosp == "None" or old_hosp == None):
                        old_hosp = 0
                    stateDict[r]["Daily_Hosp"] = int(stateDict[r]["Total_Hosp"]) - int(old_hosp)

                if stateDict[r]["Daily_Deaths"] == "None":          
                    old_deaths = layer.query(where=where, out_fields=['Total_Deaths'], return_geometry=False)
                    old_deaths = old_deaths.value['features'][0]['attributes']['Total_Deaths']
                    if(old_deaths == "None" or old_deaths == None):
                        old_deaths = 0
                    stateDict[r]["Daily_Deaths"] = int(stateDict[r]["Total_Deaths"]) - int(old_deaths)

                if stateDict[r]["Daily_Recovered"] == "None":          
                    old_rcvr = layer.query(where=where, out_fields=['Total_Recovered'], return_geometry=False)
                    old_rcvr = old_rcvr.value['features'][0]['attributes']['Total_Recovered']
                    if(old_rcvr == "None" or old_rcvr == None):
                        old_rcvr = 0
                    stateDict[r]["Daily_Recovered"] = int(stateDict[r]["Total_Recovered"]) - int(old_rcvr)   
                


                layer.calculate(where=where, 
                    calc_expression=[ {"field": "Total_New", "value": int(stateDict[r]["Total_New"])},
                                      {"field": "Daily_New", "value": int(stateDict[r]["Daily_New"])},
                                      {"field": "Total_Deaths", "value": int(stateDict[r]["Total_Deaths"])},
                                      {"field": "Daily_Deaths", "value": int(stateDict[r]["Daily_Deaths"])},
                                      {"field": "Total_Hosp", "value": int(stateDict[r]["Total_Hosp"])},
                                      {"field": "Daily_Hosp", "value": int(stateDict[r]["Daily_Hosp"])},
                                      {"field": "Total_Recovered", "value": int(stateDict[r]["Total_Recovered"])},
                                      {"field": "Daily_Recovered", "value": int(stateDict[r]["Daily_Recovered"])},
                                      {"field": "Date", "value": stateDict[r]['Date']}])
        where = "NUTS0 = '{0}'".format(ref['NUTS0'])
        state_centroids = layers[1].query(where=where, return_geometry=False)
        table = getTable(gis, tableid, index)
        print("    Updating Rolling State table...")
        table.edit_features(adds = state_centroids)
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")

            



def updateCountryLayers(country, gis, itemid, ref, tableid, index):
    #Calculates fields of state and state_centroids to update their figures. 
    #Calculates Daily values by querying old values and finding differences
    print("    Updating Country layers on portal...")
    layers = getLayers(gis, itemid, [3,1])

    layer_mod_date = layers[0].query(where=f"NUTS = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = dt.datetime.strptime(country['Date'], "%Y-%m-%dT%H:%M:%S")
    if ( file_mod_date > layer_mod_date ):
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")
                
        for layer in layers:
            # print(layer)
            #print(r)
            where = "NUTS = '{0}'".format(ref['NUTS0'])
            #Calculate Daily values by querying the layer first
            if country["Daily_Hosp"] == "None":          
                    old_hosp = layer.query(where=where, out_fields=['Total_Hosp'], return_geometry=False)
                    old_hosp = old_hosp.value['features'][0]['attributes']['Total_Hosp']
                    if(old_hosp == "None" or old_hosp == None):
                        old_hosp = 0
                    country["Daily_Hosp"] = int(country["Total_Hosp"]) - int(old_hosp)

            if country["Daily_Deaths"] == "None":          
                old_deaths = layer.query(where=where, out_fields=['Total_Deaths'], return_geometry=False)
                old_deaths = old_deaths.value['features'][0]['attributes']['Total_Deaths']
                if(old_deaths == "None" or old_deaths == None):
                    old_deaths = 0
                country["Daily_Deaths"] = int(country["Total_Deaths"]) - int(old_deaths)

            if country["Daily_Recovered"] == "None":          
                old_rcvr = layer.query(where=where, out_fields=['Total_Recovered'], return_geometry=False)
                old_rcvr = old_rcvr.value['features'][0]['attributes']['Total_Recovered']
                if(old_rcvr == "None" or old_rcvr == None):
                    old_rcvr = 0
                country["Daily_Recovered"] = int(country["Total_Recovered"]) - int(old_rcvr) 
                    
            #Do the field calculations here    
            layer.calculate(where=where, 
                calc_expression=[     {"field": "Total_New", "value": int(country["Total_New"])},
                                      {"field": "Daily_New", "value": int(country["Daily_New"])},
                                      {"field": "Total_Deaths", "value": int(country["Total_Deaths"])},
                                      {"field": "Daily_Deaths", "value": int(country["Daily_Deaths"])},
                                      {"field": "Total_Hosp", "value": int(country["Total_Hosp"])},
                                      {"field": "Daily_Hosp", "value": int(country["Daily_Hosp"])},
                                      {"field": "Total_Recovered", "value": int(country["Total_Recovered"])},
                                      {"field": "Daily_Recovered", "value": int(country["Daily_Recovered"])},
                                      {"field": "Date", "value": country['Date']}])
        #Update the rolling COUNTRY table by querying country_centroids for the NUTS0 
        where = "NUTS = '{0}'".format(ref['NUTS0'])
        country_centroids = layers[1].query(where=where, return_geometry=False)
        table = getTable(gis, tableid, index)
        print("    Updating Rolling Country table...")
        table.edit_features(adds = country_centroids)  
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
        


def job():

    
    scratchFS = '2a0326b454174cada1c30d397316717a'
    scratchTB = 'a0d0e429406f4063a1ce541e0a60ffdb'
    
    NutsFS = 'f47c01c969b14fc69980b3211b6ca542'
    NutsRollingState = '7a4e41403c1144a99cf5462bdb857756'
    NutsRollingCountry = 'f115f83f630f4fbe8eeb4d6124853c3d'
    
    global username
    global password
    

    gis = connectGIS(username, password)
    
    
    countries = {'Italy': {}}

    c_ref = {'Italy':{'type':'Editing','StateName':'denominazione_regione', 'NUTS0':'IT', 'Total_New':'totale_casi', 'Daily_New': 'nuovi_positivi',
                    'Total_Hosp':'totale_ospedalizzati', 'Total_Deaths': 'deceduti', 'Total_Recovered': 'dimessi_guariti', 'Date': 'data'}}
    
    for c in countries: 
        # Path to the JSON file
        path = os.path.join(os.path.dirname(__file__), '../json/', c , c + "Scrape_")
        file_list = glob(f"{path}202?????.json")
        file_list.sort()
        # Usually we will only process data from the most recent file:
        # file_list must remain a list so be sure to keep the outer [ ]
        # Comment out the following line to attempt to process all data files
        # Dates in State and Country tables must be manually back-dated
        # to re-process data that already exists in the tables.  You will 
        # probably want to delete rows from Rolling_State and Rolling_Country_3day
        # also to prevent duplicate entries
        #file_list = [file_list[-1]]

        ## Below used to back-fill NUTS
        for file in file_list:
            print("\n## Process Started for {0} data from {1} ##".format(c, file.split('\\')[-1] ))

            # Parses the JSON and gets all the values into a dictionary
            [states, country] = processDataFile(file, c_ref[c])

            print(" # UPDATING SCRATCH #")
            updateStateLayers(states, gis, scratchFS, c_ref[c], scratchTB, index=2)
            updateCountryLayers(country, gis, scratchFS, c_ref[c], scratchTB, index=1)

            production=False
            if production:
                print(" # UPDATING PRODUCTION #")
                updateStateLayers(states, gis, NutsFS, c_ref[c], NutsRollingState, index=0)
                updateCountryLayers(country, gis, NutsFS, c_ref[c], NutsRollingCountry, index=0)

            # print("Process Complete for {0}".format(c))
            print("## Process Complete for {0} data from {1} ##\n".format(c, file.split('\\')[-1] ))
        ## Above used to back-fill NUTS

        # my_path = os.path.abspath(os.path.dirname(__file__))
        # datafile= os.path.join(my_path, '../json', c, c + 'Scrape_' + fdate + '.json')
        # print(datafile)
        # [states, country] = processDataFile(datafile, date, c_ref[c])
        # countries[c] = states
        # # updateStateLayers(states, gis, scratchFS, c_ref[c], scratchTB)
        # # updateCountryLayers(country, gis, scratchFS, c_ref[c], scratchTB)
        # print("## Process Complete for {0} data from {1} ##".format(c, file.split('\\')[-1] ))
        
    #print(countries)

job()

