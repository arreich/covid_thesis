# To add a new cell, type ' '
# To add a new markdown cell, type '  [markdown]'

 
import datetime as dt
from datetime import datetime
from datetime import timedelta
import requests
import urllib, json, sys, collections
from arcgis.gis import GIS
from arcgis.gis import Layer
from arcgis.features import FeatureLayerCollection
from arcgis.features import Table
from arcgis.features import FeatureLayer
import os
from glob import glob
from credentials import *


 
## Returns the state_poly and state_centroid layers
def getLayers(gis, itemid, indices):
    print("Getting Layers from portal...")
    item = gis.content.get(itemid)
    poly = FeatureLayer.fromitem(item, indices[0]) 
    centroids = FeatureLayer.fromitem(item, indices[1]) 
    return [poly, centroids]


 
#Returns the table by tableid and index
def getTable(gis, tableid, index):
    print("Getting Rolling Table...")
    item = gis.content.get(tableid)
    table = Table.fromitem(item, index)
    return table


 
#Establishes connection to the 60th GPC Portal
def connectGIS(username, password):
    print("Connecting to Portal...")
    gis = GIS("https://v0yager.maps.arcgis.com/home/", username= username, password = password)
    return gis


 
#Set initial dictionary to compute values
def setStateDict(data, ref):
    stateDict = dict() 
    for county in data[ref['StateName']]:
        if not county in stateDict.keys():
            if county == 'Info puudulik':
                continue
            county = county.replace('-','')
            stateDict[county] = {'Total_New': 0, 'Daily_New': 0, 'Date': ''}
    return stateDict


 
def setCountryDict():
    countryDict = {'Total_New':0, 'Daily_New':0, 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Total_Hosp': 0, 'Daily_Hosp': 0, 'Total_Recovered': 0, 'Daily_Recovered': 0, 'Date': ''}
    return countryDict
    

#Convert unix time stamp to a datetime
def convertUnixTime(date):
    ts = int(date)
    format_date = dt.datetime.utcfromtimestamp(int(ts/1000))
    d = format_date.strftime('%m/%d/%Y, %H:%M:%S')
    return d.split(',')[0]
 
# Parse JSON file and compute values. Puts in a placeholder on Daily values until the update, 
# where we will query the old values from the layer before updating it
def processDataFile(dataJSON, ref):
    print("Processing JSON file...")
    with open(dataJSON) as datafile:
        r_data = datafile.read()
    data = json.loads(r_data)
    #print(data)
    states = setStateDict(data, ref)
    country = setCountryDict()
    date = data["updatedOn"]
    dt_date = dt.datetime.strptime(date, "%d/%m/%Y, %H:%M") - dt.timedelta(days=1)
    fdate = dt.datetime.strftime(dt_date, "%Y-%m-%d")
    calcdate = dt.datetime.strftime(dt_date, "%m/%d/%Y, %H:%M")
    #Read country data
    country['Total_New'] = data[ref['Total_New']]
    country['Daily_New'] = data[ref['Daily_New']]
    country['Total_Deaths'] = data[ref['Total_Deaths']]
    country['Daily_Deaths'] = data[ref['Daily_Deaths']]
    country['Total_Hosp'] = data[ref['Total_Hosp']]
    country['Daily_Hosp'] = data[ref['Daily_Hosp']]
    country['Total_Recovered'] = data[ref['Total_Recovered']]
    country['Daily_Recovered'] = data[ref['Daily_Recovered']]
    country['Date'] = calcdate
    
    #Read State data 
    
    
    
    
    datelist = data[ref['Date']]
    ind = datelist.index(fdate)
    
    for county in states.keys():
        states[county]['Total_New'] = data["countyByDay"]["countyByDay"][county][ind]
        states[county]['Daily_New'] = states[county]['Total_New'] - data["countyByDay"]["countyByDay"][county][ind - 1]
        states[county]['Date'] = calcdate
   
    return [states,country]


 
#Calculates fields of state and state_centroids to update their figures. 
#Calculates Daily values by querying old values and finding differences
def updateStateLayers(stateDict, gis, itemid, ref, tableid):
    print("    Updating State layers on portal...")
    layers = getLayers(gis, itemid, [2,0])
    layer_mod_date = layers[0].query(where=f"NUTS0 = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = stateDict[list(stateDict.keys())[0]]['Date']
    if ( file_mod_date > layer_mod_date ): 
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")  
 
        for layer in layers:
            print(layer)
            for r in stateDict.keys():
                where = "NUTS0 = '{0}' AND NAME = '{1}'".format(ref['NUTS0'],r)
                #Do the field calculations here
                layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(stateDict[r]["Total_New"])},
                                                                {"field": "Daily_New", "value": int(stateDict[r]["Daily_New"])},
                                                                {"field": "Date", "value": stateDict[r]['Date']}])
                
        #Update the rolling state table by querying state_centroids for the NUTS0 
        where = "NUTS0 = '{0}'".format(ref['NUTS0'])
        state_centroids = layers[1].query(where=where, return_geometry=False)
        scratch = 2
        live = 0
        table = getTable(gis, tableid, scratch)
        print("Updating Rolling State table...")
        table.edit_features(adds = state_centroids)

    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
      
    


 
def updateCountryLayers(country, gis, itemid, ref, tableid):
    #Calculates fields of state and state_centroids to update their figures. 
    #Calculates Daily values by querying old values and finding differences
    print("Updating Country layers on portal...")
    layers = getLayers(gis, itemid, [3,1])
    layers = getLayers(gis, itemid, [3,1])
    layer_mod_date = layers[0].query(where=f"NUTS = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = country['Date']
    if ( file_mod_date > layer_mod_date ):
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")
                
        for layer in layers:
            print(layer)
            #print(r)
            where = "NUTS = '{0}'".format(ref['NUTS0'])
            #Do the field calculations here    
            layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(country["Total_New"])},
                                                        {"field": "Daily_New", "value": int(country["Daily_New"])}, 
                                                        {"field": "Total_Deaths", "value": int(country["Total_Deaths"])},
                                                        {"field": "Daily_Deaths", "value": int(country["Daily_Deaths"])},
                                                        {"field": "Total_Hosp", "value": int(country["Total_Hosp"])},
                                                        {"field": "Daily_Hosp", "value": int(country["Daily_Hosp"])},
                                                        {"field": "Total_Recovered", "value": int(country["Total_Recovered"])},
                                                        {"field": "Daily_Recovered", "value": int(country["Daily_Recovered"])},
                                                        {"field": "Date", "value": country['Date']}])
        #Update the rolling COUNTRY table by querying country_centroids for the NUTS0 
        where = "NUTS = '{0}'".format(ref['NUTS0'])
        country_centroids = layers[1].query(where=where, return_geometry=False)
        scratch = 1
        live = 0
        table = getTable(gis, tableid, scratch)
        print("Updating Rolling Country table...")
        table.edit_features(adds = country_centroids)  
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
        

 
def job():
    
    scratchFS = '2a0326b454174cada1c30d397316717a'
    scratchTB = 'a0d0e429406f4063a1ce541e0a60ffdb'
    
    NutsFS = 'f47c01c969b14fc69980b3211b6ca542'
    NutsRollingState = '7a4e41403c1144a99cf5462bdb857756'
    NutsRollingCountry = 'f115f83f630f4fbe8eeb4d6124853c3d'

    
    global username
    global password
    
    gis = connectGIS(username, password)
        
    
    #Sets the country we are updating (meant to expand later, possibly)
    countries = {'Estonia': {}}
    # Use this dictionary to set the field names for the values we are interested in. 
    # Type refers to whether it's a rolling table or edited values updated each day. This would be referenced when dealing with multiple countries
    # If the service changes its field names, ONLY CHANGE IT HERE. The rest of the script references into this dictionary
    # For example, if the 'Total_New' field suddenly changes to 'Fallen' or something, just change 'Fallzahl' to that new name
    c_ref = {'Estonia':{'type':'Rolling', 'Date':'dates2', 'StateName':'counties', 'NUTS0':'EE', 'Total_New':'confirmedCasesNumber', 
                        'Total_Deaths': 'deceasedNumber', 'Total_Hosp': 'hospitalisedNumber', 'Total_Recovered': 'recoveredNumber', 
                       'Daily_New': 'activeChanged', 'Daily_Deaths': 'deceasedChanged', 'Daily_Hosp': 'hospitalChanged', 'Daily_Recovered': 'recoveredChanged'}}

    #Runs the updates
    for c in countries: 
        # Path to the JSON file
        path = os.path.join(os.path.dirname(__file__), '../json/', c , c + "Scrape_")
        file_list = glob(f"{path}202?????.json")
        file_list.sort()
        # Usually we will only process data from the most recent file:
        # file_list must remain a list so be sure to keep the outer [ ]
        # Comment out the following line to attempt to process all data files
        # Dates in State and Country tables must be manually back-dated
        # to re-process data that already exists in the tables.  You will 
        # probably want to delete rows from Rolling_State and Rolling_Country_3day
        # also to prevent duplicate entries
        #file_list = [file_list[-1]]

        ## Below used to back-fill NUTS
        for file in file_list:
            print("\n## Process Started for {0} data from {1} ##".format(c, file.split('\\')[-1] ))

            # Parses the JSON and gets all the values into a dictionary
            [states, country] = processDataFile(file, c_ref[c])
            #sets the dictionary as the result for that country (for future expansion)
            countries[c] = country
            #Update the layers on the portal
            #Change the itemids from scratch to live 
            updateStateLayers(states, gis, scratchFS, c_ref[c], scratchTB)
            updateCountryLayers(country, gis, scratchFS, c_ref[c], scratchTB)
            print("Process Complete for {0}".format(c))
        
    #print(countries)

job()

