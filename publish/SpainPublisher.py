# To add a new cell, type ' '
# To add a new markdown cell, type '  [markdown]'

 
import datetime as dt
from datetime import datetime
from datetime import timedelta
import requests
import urllib, json, sys, collections
from arcgis.gis import GIS
from arcgis.gis import Layer
from arcgis.features import FeatureLayerCollection
from arcgis.features import Table
from arcgis.features import FeatureLayer
from credentials import *
import os
from glob import glob


 
## Returns the state_poly and state_centroid layers
def getLayers(gis, itemid, indices):
    print("Getting Layers from portal...")
    item = gis.content.get(itemid)
    poly = FeatureLayer.fromitem(item, indices[0]) 
    centroids = FeatureLayer.fromitem(item, indices[1]) 
    return [poly, centroids]


 
#Returns the table by tableid and index
def getTable(gis, tableid, index):
    print("Getting Rolling Table...")
    item = gis.content.get(tableid)
    table = Table.fromitem(item, index)
    return table


 
def connectGIS(username, password):
    print("Connecting to Portal...")
    gis = GIS("https://v0yager.maps.arcgis.com/home/", username= username, password = password)
    return gis


 
def setStateDict(data, ref):
    stateDict = dict() 
    for f in data["features"]:
        for p in ref['props']:
            try:
                props = f[p]
            except:
                pass
        if not props[ref['StateName']] in stateDict.keys():
            stateDict[props[ref['StateName']]] = {'Total_New': 0, 'Daily_New': 0, 'Total_Recovered': 0, 'Daily_Recovered': 0, 
                                                 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Date': '', 'Total_Hosp': 0, 'Daily_Hosp': 0}
    return stateDict

 
def setCountryDict():
    countryDict = {'Total_New':0, 'Daily_New':0, 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Total_Recovered': 0, 'Daily_Recovered':0, 'Date': '', 'Total_Hosp': 0, 'Daily_Hosp': 0}
    return countryDict

 
#Convert unix time stamp to a datetime
def convertUnixTime(date):
    ts = int(date)
    format_date = dt.datetime.utcfromtimestamp(int(ts/1000))
    return format_date.strftime('%m/%d/%Y, %H:%M:%S')

 
def processDataFile(dataJSON, date, ref):
    print("Processing JSON file...")
    with open(dataJSON) as datafile:
        r_data = datafile.read()
    data = json.loads(r_data)
    #print(data)
    states = setStateDict(data, ref) 
    country = setCountryDict()
    ##Check for date because this one is a rolling table

    for f in data["features"]:
        for p in ref['props']:
            try:
                props = f[p]
            except:
                pass
        for input_date in ref['Date']:
            try:
                d = props[input_date].split(' ')[0]
                break
            except:
                try: 
                    d = convertUnixTime(props[input_date])
                except:
                    pass
                
        data_date = d.split(',')[0]
        if(data_date == date):
            if 'Total_New' in ref.keys():
                for total_new_key in ref['Total_New']:
                    try: 
                        states[props[ref['StateName']]]["Total_New"] += props[total_new_key]
                        country['Total_New'] += props[total_new_key]
                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Total_New"] += 0
                country['Total_New'] += 0
            if 'Total_Deaths' in ref.keys():
                for total_death_key in ref['Total_Deaths']:
                    try: 
                        states[props[ref['StateName']]]["Total_Deaths"] += props[total_death_key]
                        country['Total_Deaths'] += props[total_death_key]
                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Total_Deaths"] += 0
                country['Total_Deaths'] += 0

            if 'Daily_Deaths' in ref.keys():
                for daily_death_key in ref['Daily_Deaths']:
                    try: 
                        states[props[ref['StateName']]]["Daily_Deaths"] += props[daily_death_key]
                        country['Daily_Deaths'] += props[daily_death_key]
                        if (props[daily_death_key] == "None" or props[daily_death_key] == None):
                            states[props[ref['StateName']]]["Daily_Deaths"] = "None"
                            country['Daily_Deaths'] = "None"
                        
                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Daily_Deaths"] = "None"
                country['Daily_Deaths'] = "None"

            if 'Daily_New' in ref.keys():
                for daily_new_key in ref['Daily_New']:
                    try: 
                        states[props[ref['StateName']]]["Daily_New"] += props[daily_new_key]
                        country['Daily_New'] += props[daily_new_key]
                        if (props[daily_new_key] == "None" or props[daily_new_key] == None):
                            states[props[ref['StateName']]]["Daily_New"] = "None"
                            country['Daily_New'] = "None"
                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Daily_New"] = "None"
                country['Daily_New'] = "None"
            if 'Total_Recovered' in ref.keys():
                for total_recover_key in ref['Total_Recovered']:
                    try: 
                        states[props[ref['StateName']]]["Total_Recovered"] += props[total_recover_key]
                        country['Total_Recovered'] += props[total_recover_key]
                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Total_Recovered"] = "None"
                country['Total_Recovered'] = "None"

            if 'Daily_Recovered' in ref.keys():
                for daily_recover_key in ref['Daily_Recovered']:
                    try: 
                        states[props[ref['StateName']]]["Daily_Recovered"] += props[daily_recover_key]
                        country['Daily_Recovered'] += props[daily_recover_key]
                        if (props[daily_recover_key] == "None" or props[daily_recover_key] == None):
                            states[props[ref['StateName']]]["Daily_Recovered"] = "None"
                            country['Daily_Recovered'] = "None"

                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Daily_Recovered"] = "None"
                country['Daily_Recovered'] = "None"

            if 'Total_Hosp' in ref.keys():
                for total_hosp_key in ref['Total_Hosp']:
                    try: 
                        states[props[ref['StateName']]]["Total_Hosp"] += props[total_hosp_key]
                        country['Total_Hosp'] += props[total_hosp_key]
                    except:
                        pass
            else:
                states[props[ref['StateName']]]["Total_Hosp"] += 0
                country['Total_Hosp'] += 0

            states[props[ref['StateName']]]['Daily_Hosp'] = "None"
            country['Daily_Hosp'] = "None"

            states[props[ref['StateName']]]["Date"] = data_date
            country['Date'] = data_date
            
    return [states, country]


 
def updateStateLayers(stateDict, gis, itemid, ref, tableid, index):
    print("    Updating State layers on portal...")
    layers = getLayers(gis, itemid, [2,0])
    layer_mod_date = layers[0].query(where=f"NUTS0 = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    ind = 0
    while(stateDict[list(stateDict.keys())[ind]]['Date'] == ''):    
        ind = ind + 1
    file_mod_date = stateDict[list(stateDict.keys())[ind]]['Date']
    if ( file_mod_date > layer_mod_date ):  
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")  

        for layer in layers:
            print(layer)
            for r in stateDict.keys():
                print(r)
                print(stateDict[r])
                where = "NUTS0 = '{0}' AND NAME = '{1}'".format(ref['NUTS0'],r)
                if stateDict[r]['Date'] == '':
                    stateDict[r]['Date'] = file_mod_date
                if stateDict[r]["Daily_New"] == "None":          
                    old_new = layer.query(where=where, out_fields='Total_New', return_geometry=False)
                    old_new = old_new.value['features'][0]['attributes']['Total_New']
                    if(old_new == "None"):
                        old_new = 0
                    stateDict[r]["Daily_New"] = int(stateDict[r]["Total_New"]) - int(old_new)
                        
                if stateDict[r]["Daily_Deaths"] == "None":
                    old_deaths = layer.query(where=where, out_fields='Total_Deaths', return_geometry=False)
                    old_deaths = str(old_deaths.value['features'][0]['attributes']['Total_Deaths'])
                    if(old_deaths == "None"):
                        old_deaths = 0
                    stateDict[r]["Daily_Deaths"] = int(stateDict[r]["Total_Deaths"]) - int(old_deaths)
                    #print(stateDict[r]["Daily_Deaths"])
                if stateDict[r]["Daily_Recovered"] == "None":
                    old_rcvr = layer.query(where=where, out_fields='Total_Recovered', return_geometry=False)
                    old_rcvr = str(old_rcvr.value['features'][0]['attributes']['Total_Recovered'])
                    if(old_rcvr == "None"):
                        old_rcvr = 0
                    stateDict[r]["Daily_Recovered"] = int(stateDict[r]["Total_Recovered"]) - int(old_rcvr)
                    #print(stateDict[r]["Daily_Hosp"])
                
                
                print(layer.validate_sql(where, sql_type='where'))
                success = layer.calculate(where=where, calc_expression=[{"field": "Total_New", "value": int(stateDict[r]["Total_New"])},
                                                                        {"field": "Daily_New", "value": int(stateDict[r]["Daily_New"])}, 
                                                                        {"field": "Total_Deaths", "value": int(stateDict[r]["Total_Deaths"])},
                                                                        {"field": "Daily_Deaths", "value": int(stateDict[r]["Daily_Deaths"])},
                                                                        {"field": "Total_Recovered", "value": int(stateDict[r]["Total_Recovered"])},
                                                                        {"field": "Daily_Recovered", "value": int(stateDict[r]["Daily_Recovered"])},
                                                                        {"field": "Date", "value": stateDict[r]["Date"]}])
        where = "NUTS0 = '{0}'".format(ref['NUTS0'])
        state_centroids = layers[1].query(where=where, return_geometry=False)
        
        table = getTable(gis, tableid, index)
        print("Updating Rolling State table...")
        table.edit_features(adds = state_centroids)
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")

            
 
def updateCountryLayers(country, gis, itemid, ref, tableid, index):
    #Calculates fields of state and state_centroids to update their figures. 
    #Calculates Daily values by querying old values and finding differences
    print("Updating Country layers on portal...")
    layers = getLayers(gis, itemid, [3,1])
    layer_mod_date = layers[0].query(where=f"NUTS = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = country['Date']
    if ( file_mod_date > layer_mod_date ):
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")
                
        for layer in layers:
            print(layer)
            where = "NUTS = '{0}'".format(ref['NUTS0'])
            #Calculate Daily values by querying the layer first
            if country["Daily_New"] == "None":          
                old_new = layer.query(where=where, out_fields='Total_New', return_geometry=False)
                old_new = old_new.value['features'][0]['attributes']['Total_New']
                if(old_new == None):
                    old_new = 0
                country["Daily_New"] = int(country["Total_New"]) - int(old_new)
                
            if country["Daily_Deaths"] == "None":
                old_deaths = layer.query(where=where, out_fields='Total_Deaths', return_geometry=False)
                old_deaths = old_deaths.value['features'][0]['attributes']['Total_Deaths']
                if(old_deaths == None):
                    old_deaths = 0
                country["Daily_Deaths"] = int(country["Total_Deaths"]) - int(old_deaths)
                
            if country["Daily_Recovered"] == "None":
                old_rcvr = layer.query(where=where, out_fields='Total_Recovered', return_geometry=False)
                old_rcvr = old_rcvr.value['features'][0]['attributes']['Total_Recovered']
                if(old_rcvr == None):
                    old_rcvr = 0
                country['Daily_Recovered'] = int(country['Total_Recovered']) - int(old_rcvr)
                    
            #Do the field calculations here    
            success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(country["Total_New"])},
                                                                    {"field": "Daily_New", "value": int(country["Daily_New"])}, 
                                                                    {"field": "Total_Deaths", "value": int(country["Total_Deaths"])},
                                                                    {"field": "Daily_Deaths", "value": int(country["Daily_Deaths"])},
                                                                    {"field": "Total_Recovered", "value": int(country["Total_Recovered"])}, 
                                                                    {"field": "Daily_Recovered", "value": int(country["Daily_Recovered"])},
                                                                    {"field": "Date", "value": country['Date']}])
        #Update the rolling COUNTRY table by querying country_centroids for the NUTS0 
        where = "NUTS = '{0}'".format(ref['NUTS0'])
        country_centroids = layers[1].query(where=where, return_geometry=False)
        
        table = getTable(gis, tableid, index)
        print("Updating Rolling Country table...")
        table.edit_features(adds = country_centroids)  
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
        

 
def job():

    global username
    global password

     
    scratchFS = '2a0326b454174cada1c30d397316717a'
    scratchTB = 'a0d0e429406f4063a1ce541e0a60ffdb'
    
    NutsFS = 'f47c01c969b14fc69980b3211b6ca542'
    NutsRollingState = '7a4e41403c1144a99cf5462bdb857756'
    NutsRollingCountry = 'f115f83f630f4fbe8eeb4d6124853c3d'

    gis = connectGIS(username, password)
    
    
    countries = {'Spain': {}}
    c_ref = {'Spain':{'type':'Rolling','StateName':'NombreCCAA', 'NUTS0':'ES', 'props': ['properties', 'attributes'], 'Date': ['FechaNormalizada', 'Fecha'], 
                        'Total_New':['TotalConfirmados', 'CasosConfirmados'],'Total_Deaths': ['TotalFallecidos', 'Fallecidos'], 
                        'Total_Recovered': ['TotalRecuperados', 'Recuperados'], 'Daily_New': ['NuevosConfirmados', 'NuevoCasos'], 
                        'Daily_Deaths': ['NuevosFallecidos'], 'Daily_Recovered':'NuevosRecuperados', 'Total_Hosp': 'Hospitalizados'}}
    
    for c in countries: 
        path = os.path.join(os.path.dirname(__file__), '../json/', c , c + "Scrape_")
        file_list = glob(f"{path}202?????.json")
        file_list.sort()
        # Usually we will only process data from the most recent file:
        # file_list must remain a list so be sure to keep the outer [ ]
        # Comment out the following line to attempt to process all data files
        # Dates in State and Country tables must be manually back-dated
        # to re-process data that already exists in the tables.  You will 
        # probably want to delete rows from Rolling_State and Rolling_Country_3day
        # also to prevent duplicate entries
        #file_list = [file_list[-1]]
        for file in file_list:
            date = os.path.basename(file).split('_')[1]
            date = date.split('.')[0]
            date = dt.datetime.strptime(date, '%Y%m%d') - dt.timedelta(days=1)
            date = dt.datetime.strftime(date, '%m/%d/%Y')
            print(date)
            [states, country] = processDataFile(file, date, c_ref[c])
            countries[c] = states
            print(states)

            print(" # UPDATING SCRATCH #")
            updateStateLayers(states, gis, scratchFS, c_ref[c], scratchTB, index=2)
            updateCountryLayers(country, gis, scratchFS, c_ref[c], scratchTB, index=1)

            production=False
            if production:
                print(" # UPDATING PRODUCTION #")
                updateStateLayers(states, gis, NutsFS, c_ref[c], NutsRollingState, index=0)
                updateCountryLayers(country, gis, NutsFS, c_ref[c], NutsRollingCountry, index=0)

                # print("Process Complete for {0}".format(c))
            print("## Process Complete for {0} data from {1} ##\n".format(c, file.split('\\')[-1] ))

job()
