# To add a new cell, type ' '
# To add a new markdown cell, type '  [markdown]'

 
import datetime as dt
from datetime import datetime
from datetime import timedelta
import requests
import urllib, json, sys, collections
from arcgis.gis import GIS
from arcgis.gis import Layer
from arcgis.features import FeatureLayerCollection
from arcgis.features import Table
from arcgis.features import FeatureLayer
from credentials import *
import os
from glob import glob


 
## Returns the state_poly and state_centroid layers
def getLayers(gis, itemid, indices):
    print("Getting Layers from portal...")
    item = gis.content.get(itemid)
    poly = FeatureLayer.fromitem(item, indices[0]) 
    centroids = FeatureLayer.fromitem(item, indices[1]) 
    return [poly, centroids]


 
#Returns the table by tableid and index
def getTable(gis, tableid, index):
    print("Getting Rolling Table...")
    item = gis.content.get(tableid)
    table = Table.fromitem(item, index)
    return table


 
def connectGIS(username, password):
    print("Connecting to Portal...")
    gis = GIS("https://v0yager.maps.arcgis.com/home/", username= username, password = password)
    return gis



 
#Set initial dictionary to compute values
def setStateDict(data, ref):
    stateDict = dict() 
    for f in data["features"]:
        props = f["properties"]
        if not props[ref['StateName']] in stateDict.keys():
            stateDict[props[ref['StateName']]] = {'Total_New': 0, 'Daily_New': 0, 
                                                 'Total_Deaths': 0, 'Daily_Deaths': 0}

    return stateDict

 
def setCountryDict():
    countryDict = {'Total_New':0, 'Daily_New':0, 'Total_Deaths': 0, 'Daily_Deaths': 0, 'Total_Hosp': 0, 'Daily_Hosp':0, 'Date': ''}
    return countryDict

 
#Convert unix time stamp to a datetime
def convertUnixTime(date):
    if isinstance(date, int):
        ts = int(date)
        format_date = dt.datetime.utcfromtimestamp(int(ts/1000))
    else:
        format_date = date
    return format_date.strftime('%m/%d/%Y, %H:%M:%S')


 
# Parse JSON file and compute values. Puts in a placeholder on Daily values until the update, 
# where we will query the old values from the layer before updating it
def processDataFile(dataJSON, date, ref):
    print("Processing JSON file...")
    with open(dataJSON) as datafile:
        r_data = datafile.read()
    data = json.loads(r_data)
    #print(data)
    states = setStateDict(data, ref)
    country = setCountryDict() 
   
    for f in data["features"]:
        props = f["properties"]
        if 'Total_New' in ref.keys():
            if not props[ref['Total_New']] == None:
                states[props[ref['StateName']]]["Total_New"] += props[ref['Total_New']]
                country['Total_New'] += props[ref['Total_New']]
            else:
                states[props[ref['StateName']]]["Total_New"] += 0
                country['Total_New'] += 0
        else:
            states[props[ref['StateName']]]["Total_New"] += 0
            country['Total_New'] += 0
        if 'Total_Deaths' in ref.keys():
            if not props[ref['Total_Deaths']] == None:
                states[props[ref['StateName']]]["Total_Deaths"] += props[ref['Total_Deaths']]
                country['Total_Deaths'] += props[ref['Total_Deaths']]
            else:
                states[props[ref['StateName']]]["Total_Deaths"] += 0
                country['Total_Deaths'] += 0
        else:
            states[props[ref['StateName']]]["Total_Deaths"] += 0
            country['Total_Deaths'] += 0
        if 'Date' in ref.keys():
            states[props[ref['StateName']]]["Date"] = convertUnixTime(props[ref['Date']])
            country['Date'] = convertUnixTime(props[ref['Date']])
        else:
            states[props[ref['StateName']]]["Date"] = date
            country['Date'] = date
        if 'Daily_Deaths' in ref.keys():
            states[props[ref['StateName']]]["Daily_Deaths"] += props[ref['Daily_Deaths']]
            country['Daily_Deaths'] += props[ref['Daily_Deaths']]
        else:
            states[props[ref['StateName']]]["Daily_Deaths"] = "None"
            country['Daily_Deaths'] = "None"
        if 'Daily_New' in ref.keys():
            states[props[ref['StateName']]]["Daily_New"] += props[ref['Daily_New']]
            country['Daily_New'] += props[ref['Daily_New']]
        else:
            states[props[ref['StateName']]]["Daily_New"] = "None"
            country['Daily_New'] = "None"
            
    return [states, country]


 
#Calculates fields of state and state_centroids to update their figures. 
#Calculates Daily values by querying old values and finding differences
def updateStateLayers(stateDict, gis, itemid, ref, tableid, index):
    print("Updating layers on portal...")
    layers = getLayers(gis, itemid, [2,0])
    layer_mod_date = layers[0].query(where=f"NUTS0 = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = convertUnixTime(stateDict[list(stateDict.keys())[0]]['Date'])
    if ( file_mod_date > layer_mod_date ):  
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")  

        for layer in layers:
            print(layer)
            for r in stateDict.keys():
                #print(r)
                where = "NUTS0 = '{0}' AND NAME = '{1}'".format(ref['NUTS0'],r)
                #Calculate Daily values by querying the layer first
                if stateDict[r]["Daily_New"] == "None":          
                    old_new = layer.query(where=where, out_fields='Total_New', return_geometry=False)
                    old_new = old_new.value['features'][0]['attributes']['Total_New']
                    if(old_new == 'None' or old_new == None):
                        old_new = 0
                    stateDict[r]["Daily_New"] = int(stateDict[r]["Total_New"]) - int(old_new)
                    #print(stateDict[r]["Daily_New"])
                if stateDict[r]["Daily_Deaths"] == "None":
                    old_deaths = layer.query(where=where, out_fields='Total_Deaths', return_geometry=False)
                    old_deaths = str(old_deaths.value['features'][0]['attributes']['Total_Deaths'])
                    if(old_deaths == 'None' or old_deaths == None):
                        old_deaths = 0
                    stateDict[r]["Daily_Deaths"] = int(stateDict[r]["Total_Deaths"]) - int(old_deaths)
                    #print(stateDict[r]["Daily_Deaths"]) 
                #print(layer.validate_sql(where, sql_type='where'))
                #Do the field calculations here
                success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(stateDict[r]["Total_New"])},
                                                                        {"field": "Daily_New", "value": int(stateDict[r]["Daily_New"])}, 
                                                                        {"field": "Total_Deaths", "value": int(stateDict[r]["Total_Deaths"])},
                                                                        {"field": "Daily_Deaths", "value": int(stateDict[r]["Daily_Deaths"])},
                                                                    {"field": "Date", "value": stateDict[r]["Date"]}])
                
        #Update the rolling state table by querying state_centroids for the NUTS0 
        where = "NUTS0 = '{0}'".format(ref['NUTS0'])
        state_centroids = layers[1].query(where=where, return_geometry=False)
        
        table = getTable(gis, tableid, index)
        print("Updating Rolling State table...")
        table.edit_features(adds = state_centroids)
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")

            
            
 
def updateCountryLayers(country, gis, itemid, ref, tableid, index):
    #Calculates fields of state and state_centroids to update their figures. 
    #Calculates Daily values by querying old values and finding differences
    print("Updating Country layers on portal...")
    layers = getLayers(gis, itemid, [3,1])
    layer_mod_date = layers[0].query(where=f"NUTS = '{ref['NUTS0']}'", out_fields='Date', return_geometry=False)
    layer_mod_date = convertUnixTime(layer_mod_date.value['features'][0]['attributes']['Date'])
    file_mod_date = convertUnixTime(country['Date'])
    if ( file_mod_date > layer_mod_date ):
        print(f"  COMMITTING - JSON data last updated {file_mod_date} is newer than table data from {layer_mod_date}.")
             
        for layer in layers:
            print(layer)
            where = "NUTS = '{0}'".format(ref['NUTS0'])
            #Calculate Daily values by querying the layer first
            if country["Daily_New"] == "None":          
                old_new = layer.query(where=where, out_fields='Total_New', return_geometry=False)
                old_new = old_new.value['features'][0]['attributes']['Total_New']
                if(old_new == 'None' or old_new == None):
                    old_new = 0
                country["Daily_New"] = int(country["Total_New"]) - int(old_new)
                
            if country["Daily_Deaths"] == "None":
                old_deaths = layer.query(where=where, out_fields='Total_Deaths', return_geometry=False)
                old_deaths = old_deaths.value['features'][0]['attributes']['Total_Deaths']
                if(old_deaths == 'None' or old_deaths == None):
                    old_deaths = 0
                country["Daily_Deaths"] = int(country["Total_Deaths"]) - int(old_deaths)
                
            #Do the field calculations here    
            success = layer.calculate(where=where, calc_expression=[ {"field": "Total_New", "value": int(country["Total_New"])},
                                                                    {"field": "Daily_New", "value": int(country["Daily_New"])}, 
                                                                    {"field": "Total_Deaths", "value": int(country["Total_Deaths"])},
                                                                    {"field": "Daily_Deaths", "value": int(country["Daily_Deaths"])},
                                                                    {"field": "Date", "value": country['Date']}])
        #Update the rolling COUNTRY table by querying country_centroids for the NUTS0 
        where = "NUTS = '{0}'".format(ref['NUTS0'])
        country_centroids = layers[1].query(where=where, return_geometry=False)
        
        table = getTable(gis, tableid, index)
        print("Updating Rolling Country table...")
        table.edit_features(adds = country_centroids)  
    else:
        print(f"  SKIPPING - JSON data last updated {file_mod_date} is not newer than table data from {layer_mod_date}.")
        


 
def job( ):

    global username
    global password    
    

    scratchFS = '2a0326b454174cada1c30d397316717a'
    scratchTB = 'a0d0e429406f4063a1ce541e0a60ffdb'
    
    NutsFS = 'f47c01c969b14fc69980b3211b6ca542'
    NutsRollingState = '7a4e41403c1144a99cf5462bdb857756'
    NutsRollingCountry = 'f115f83f630f4fbe8eeb4d6124853c3d'

    gis = connectGIS(username, password)

    countries = {'Greece': {}}
    c_ref = {'Greece':{'type':'Editing', 'StateName':'NAME', 'NUTS0':'GR', 'Total_New':'Confirmed', 'Total_Deaths': 'Deaths'}}
    
    #Runs the updates
    for c in countries: 
        path = os.path.join(os.path.dirname(__file__), '../json/', c , c + "Scrape_")
        file_list = glob(f"{path}202?????.json")
        file_list.sort()
        # Usually we will only process data from the most recent file:
        # file_list must remain a list so be sure to keep the outer [ ]
        # Comment out the following line to attempt to process all data files
        # Dates in State and Country tables must be manually back-dated
        # to re-process data that already exists in the tables.  You will 
        # probably want to delete rows from Rolling_State and Rolling_Country_3day
        # also to prevent duplicate entries
        #file_list = [file_list[-1]]

        ## Below used to back-fill NUTS
        for file in file_list:
            print("\n## Process Started for {0} data from {1} ##".format(c, file.split('\\')[-1] ))
            date = dt.datetime.strptime(file[-13:-5], "%Y%m%d")
            [states, country] = processDataFile(file, date, c_ref[c])
            countries[c] = states
            ## Update Scratch (DEV) ##
            # Index for StateLayers in scratch = 2
            # Index for CountryLayers in scratch = 1
            print(" # UPDATING SCRATCH #")
            updateStateLayers(states, gis, scratchFS, c_ref[c], scratchTB, index=2)
            updateCountryLayers(country, gis, scratchFS, c_ref[c], scratchTB, index=1)
            
            ## Update PROD/NUTS ##
            # Index for prod = 0
            production=False
            if production:
                print(" # UPDATING PRODUCTION #")
                updateStateLayers(states, gis, NutsFS, c_ref[c], NutsRollingState, index=0)
                updateCountryLayers(country, gis, NutsFS, c_ref[c], NutsRollingCountry, index=0)

            print("## Process Complete for {0} data from {1} ##\n".format(c, file.split('\\')[-1] ))
            
job()
