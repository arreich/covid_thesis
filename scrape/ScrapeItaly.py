# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
from bs4 import BeautifulSoup
import datetime
import json
from esridump.dumper import EsriDumper
import subprocess
import requests
import time
import urllib
import os


# %%
url = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni-latest.json'
my_path = os.path.abspath(os.path.dirname(__file__))
last_update = os.path.join(my_path, './updateTimes/ItalyUpdates.txt')
error_count = 0


# %%
try: 
    with open(last_update, 'r') as old:
        old_date = old.readlines()
    old_date = datetime.datetime.strptime(old_date[0], "%Y-%m-%dT%H:%M:%S")
    #print(old_date)
    print(datetime.datetime.now())
    print("Checking Updates for Italy: ")
    #retrieve JSON from URL
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    
    #extract date for latest update.
    raw_date= data[0]['data']
    print(raw_date)
    
    #get Date object from date string
    #2020-06-17T17:00:00
    date = datetime.datetime.strptime(raw_date, "%Y-%m-%dT%H:%M:%S")
    #print(date)
    if old_date < date: 
        with open(last_update, 'w') as out:
            out.write(raw_date)
        print("New Update made as of " + raw_date)
        print("Scraping JSON file for data processing")
        #format date for using in file name
        fdate = datetime.datetime.strftime(date, "%Y%m%d")
        
        #Set output file path
        
        filepath = os.path.join(my_path, '../json/Italy')

        outjson = {"type": "FeatureCollection", "features": []}
        for d in data:
            #print(d)
            outjson["features"].append({"type": "Feature", "properties": d })
        
        #Set file name
        outfile = os.path.join(filepath, 'ItalyScrape_' + fdate + '.json')
        
        #write JSON into file
        with open(outfile, 'w+') as out:
            
            out.write(json.dumps(outjson))
        print("JSON output complete")
        print("Waiting for next check...")
    else:
        print("No new updates")
        print("Waiting for next check...")
    
    
    
    #Catch and print error
    #This will try again after 5 minutes and end the loop after 5 errors"""
except Exception as e:
    print("Encountered error getting data for Italy")
    print(e)