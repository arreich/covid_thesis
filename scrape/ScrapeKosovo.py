# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
from bs4 import BeautifulSoup
import datetime
import json
from esridump.dumper import EsriDumper
import subprocess
import requests
import time
import urllib



# %%
def updateKosovoJSON(url, filename):
    print(subprocess.run(["esri2geojson", url, filename]))


# %%
def job():
    my_path = os.path.abspath(os.path.dirname(__file__))
    url = 'https://gdigeoportal.rs/server/rest/services/COVID19/COVID19/FeatureServer/39'
    today = datetime.datetime.strftime(datetime.datetime.today()-datetime.timedelta(days=1), "%Y%m%d")
    path = os.path.join(my_path, '../json/Kosovo/')
    name = 'KosovoScrape_' + today + '.json'
    updateKosovoJSON(url, path+name)


# %%
job()

