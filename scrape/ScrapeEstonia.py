# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
from bs4 import BeautifulSoup
import datetime
import json
from esridump.dumper import EsriDumper
import subprocess
import requests
import time
import urllib
import os


# %%
url = 'https://raw.githubusercontent.com/okestonia/koroonakaart/master/koroonakaart/src/data.json'
my_path = os.path.abspath(os.path.dirname(__file__))
last_update = os.path.join(my_path, './updateTimes/EstoniaUpdates.txt')
error_count = 0


# %%
try: 
    with open(last_update, 'r') as old:
        old_date = old.readlines()
    old_date = datetime.datetime.strptime(old_date[0], "%d/%m/%Y, %H:%M")
    #print(old_date)
    print(datetime.datetime.now())
    print("Checking Updates for Estonia: ")
    #retrieve JSON from URL
    response = urllib.request.urlopen(url)
    data = json.loads(response.read())
    
    #extract date for latest update.
    raw_date= data['updatedOn']
    #print(raw_date)
    
    #get Date object from date string
    date = datetime.datetime.strptime(raw_date, "%d/%m/%Y, %H:%M")
    #print(date)
    if old_date < date: 
        with open(last_update, 'w') as out:
            out.write(raw_date)
        print("New Update made as of " + raw_date)
        print("Scraping JSON file for data processing")
        #format date for using in file name
        fdate = datetime.datetime.strftime(date, "%Y%m%d")
        
        #Set output file path
        
        filepath = os.path.join(my_path, '../json/Estonia')
        
        #Set file name
        outfile = os.path.join(filepath, 'EstoniaScrape_' + fdate + '.json')
        
        #write JSON into file
        with open(filepath + outfile, 'w+') as out:
            out.write(json.dumps(data))
        print("JSON output complete")
        print("Waiting for next check...")
    else:
        print("No new updates")
        print("Waiting for next check...")
    #sleep for an hour and then run again
    
    
    #Catch and print error
    #This will try again after 5 minutes and end the loop after 5 errors
except Exception as e:
    print("Encountered error getting data for Estonia")
    print(e)
          


