# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
import bs4 
import datetime
import re
import requests
import time
import json
from esridump.dumper import EsriDumper
import subprocess
from shutil import copyfile
import os
import ScrapeSpain


# %%
services = {
    'Israel':'https://services5.arcgis.com/dlrDjz89gx9qyfev/ArcGIS/rest/services/Corona_Muni_Isolations_VIEW2/FeatureServer/0', 
    'Netherlands':'https://services.arcgis.com/nSZVuSZjHpEZZbRo/ArcGIS/rest/services/Coronavirus_RIVM_vlakken_actueel/FeatureServer/0', 
    'France':'https://services1.arcgis.com/5PzxEwuu4GtMhqQ6/ArcGIS/rest/services/Synthese_Covid19_France/FeatureServer/0',
    'Greece': 'https://services9.arcgis.com/Rha9bYQCF0JEy8bJ/ArcGIS/rest/services/Current_Coronavirus_Cases_and_Deaths/FeatureServer/0',
    'Romania':'https://services.arcgis.com/IjJbzDQF4hOiNl87/ArcGIS/rest/services/Cazuri_COVID19_la_nivel_de_jude%C8%9B_(la_data_curent%C4%83)/FeatureServer/0',
    'Ukraine':'https://services9.arcgis.com/N16ENplDulp0YdxI/arcgis/rest/services/%D0%92%D0%B8%D0%BF%D0%B0%D0%B4%D0%BA%D0%B8_COVID19_%D0%B2_%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D1%96_%D0%BF%D0%BE_%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8F%D0%BC_view/FeatureServer/0',
    'Germany':'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/ArcGIS/rest/services/Coronaf%c3%a4lle_in_den_Bundesl%c3%a4ndern/FeatureServer/0',
    'Lithuania': 'https://services.arcgis.com/XdDVrnFqA9CT3JgB/ArcGIS/rest/services/covid_locations/FeatureServer/0', 
    'Turkey':'https://services5.arcgis.com/26ObnytmBZ58Wbj8/arcgis/rest/services/Kovid_19_TurkiyeIL/FeatureServer/0', 
    'Spain': 'https://services7.arcgis.com/lTrEzFGSU2ayogtj/arcgis/rest/services/COMPLETA_Afectados_por_coronavirus_por_provincia_en_Espa%C3%B1a_Vista_Solo_lectura/FeatureServer/0'
}


my_path = os.path.abspath(os.path.dirname(__file__))
last_updates = {
    'Israel': os.path.join(my_path, './updateTimes/IsraelUpdates.txt'),
    'Netherlands': os.path.join(my_path, './updateTimes/NetherlandsUpdates.txt'), 
    'France': os.path.join(my_path, './updateTimes/FranceUpdates.txt'),
    'Greece': os.path.join(my_path, './updateTimes/GreeceUpdates.txt'),
    'Romania': os.path.join(my_path, './updateTimes/RomaniaUpdates.txt'),
    'Ukraine': os.path.join(my_path, './updateTimes/UkraineUpdates.txt'),
    'Germany': os.path.join(my_path, './updateTimes/GermanyUpdates.txt'),
    'Lithuania': os.path.join(my_path, './updateTimes/LithuaniaUpdates.txt'),
    'Turkey': os.path.join(my_path, './updateTimes/TurkeyUpdates.txt'),
    'Spain': os.path.join(my_path, './updateTimes/SpainUpdates.txt'),
    'USAREUR_State': os.path.join(my_path, './updateTimes/USAREURUpdates0.txt'),
    'USAREUR_StateC': os.path.join(my_path, './updateTimes/USAREURUpdates1.txt'),
    'USAREUR_Country': os.path.join(my_path, './updateTimes/USAREURUpdates2.txt'),
    'USAREUR_CountryC': os.path.join(my_path, './updateTimes/USAREURUpdates3.txt')
    
}
""""""

#
error_count = 0


# %%
def addGeo(jfile):
    print("Adding fake geo info to Spain to run JSON to Features")
    with open(jfile) as datafile:
        r_data = datafile.read()
    jData = json.loads(r_data)
    # loop through json
    for feature in jData['features']:
        feature['geometry'] = { "type" : "Point",
                           "coordinates" : [
                               0.0, 0.0]
                           }
    with open(jfile, 'w+') as out:
        out.write(json.dumps(jData))


# %%
def formatDate(jfile, datefield):
    print("Adjusting unix time to standard datetime")
    with open(jfile) as datafile:
        r_data = datafile.read()
    jData = json.loads(r_data)
    for feature in jData['features']:
        ts = feature['properties'][datefield]
        format_date = datetime.datetime.utcfromtimestamp(int(ts/1000))
        feature['properties'][datefield] = format_date.strftime('%m/%d/%Y, %H:%M:%S')
    with open(jfile, 'w+') as out:
        out.write(json.dumps(jData))


# %%
def filterGreece(jfile):
    print(jfile)
    with open(jfile, 'r') as datafile:
        r_data = datafile.read()
    jData = json.loads(r_data)
    new_json = {"type":"FeatureCollection","features":[]}
    for feature in jData['features']:
        if feature["properties"]["COUNTRY"] == "Greece":
            new_json["features"].append(feature)
    #print(new_json)        
    with open(jfile, 'w') as out:
        out.write(json.dumps(new_json))
    print("Done")


# %%
try:
    for c in services.keys():
        print(datetime.datetime.now())
        print("Checking Updates for " + c + ":")
        with open(last_updates[c],'r') as f:
            old_date = f.readlines()
            f.close()
        old_date = datetime.datetime.strptime(old_date[0], '%m/%d/%Y %H:%M:%S %p')
        #Get the webpage information
        page = requests.get(services[c])
        #Pull the HTML from the webpage
        text = bs4.BeautifulSoup(page.content,'html.parser')
        #Do a REGEX search for the date in the format m/d/y h:m:s AM(PM)
        #Output is a list
        webpage_time = re.findall('\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{1,2}:\d{1,2}\s[AP]M',text.get_text())
        #Set webpage time to datetime Object
        new_time = datetime.datetime.strptime(webpage_time[-1],'%m/%d/%Y %H:%M:%S %p')
        #If start date and new_time are same, do nothing
        if old_date < new_time:
            with open(last_updates[c], 'w+') as out:
                out.write(webpage_time[-1])
                out.close()
            
            print("New Update made as of " + webpage_time[-1])
            print("Scraping JSON file for data processing")
            
            update_time = datetime.datetime.strftime(new_time, "%Y%m%d")
            
            path = os.path.join(my_path,'../json/', c)
            filename = os.path.join(path, c + 'Scrape_' + update_time + '.json')
            if c == 'Spain': 
                ScrapeSpain.scrape(services[c], filename)
            else:
                print(subprocess.run(["esri2geojson", services[c], filename]))
            
            if c == 'Netherlands':
                formatDate(filename, 'Datum')
            elif c == 'Greece':
                filterGreece(filename)

        else:
            print('There have been no new updates.')
            pass
    
        
except Exception as e:
    print("Encountered error on " + c)
    print(e)
    
    



# %%
