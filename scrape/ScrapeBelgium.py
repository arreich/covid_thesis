# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
import datetime
import json
from esridump.dumper import EsriDumper
import subprocess
import requests
import time
import os

#This method is adding fake geometry to a JSON file to allow it to be read by Esri products as a GEOJSON.
#The method gives the file a geometry point of 0, 0 and will populate on SIGINT Island. :) 
def AddFakeGeo_Cases(jfile):
# create an empty dictionary of features
    jData = { "features" : None }
    jData["features"] = jfile

# start the geojson file
    geo = {
        "type" : "FeatureCollection",
        "crs" : { "type" : "name",
                  "properties" : {
                      "name" : "EPSG:4326"
                      }
                  },
        "features" : None
        }

    feature_list = []

    # loop through json
    fCount = 0
    for feature in jData['features']:
        fCount +=1
        #This will allow the process to proceed even if one field is missing.
        #If region is missing, there will be a new "Region" called NA. Check for this in the data
        #If the Deaths figure or the date are missing, it won't be useful for our data, so the loop will pass over that iteration
        if not "PROVINCE" in feature:
            feature["PROVINCE"] = "NA"
        if not "REGION" in feature:
            feature["REGION"] = "NA"
        if not "AGEGROUP" in feature:
            feature["AGEGROUP"] = "NA"
        if not "SEX" in feature:
            feature["SEX"] = "NA"
        if not "CASES" in feature: 
            continue
        if not "DATE" in feature:
            continue
        features = {
            "type" : "Feature",
            "id" : fCount,
            #Fake Geometry
            "geometry" : { "type" : "Point",
                           "coordinates" : [
                               0.0, 0.0]
                           },
            #Real properties copied over
            "properties" : { "DATE" : feature["DATE"],
                         "PROVINCE" : feature["PROVINCE"],
                         "REGION" : (feature["REGION"]),
                         "AGEGROUP" : (feature["AGEGROUP"]),
                         "SEX" : (feature["SEX"]),
                         "CASES" : int(feature["CASES"]),
                         }
            }
       
        #Add features to list   
        feature_list.append(features)

   
    #Add feature list to the GEOJSON
    geo["features"] = feature_list

    return geo

    #This method is adding fake geometry to a JSON file to allow it to be read by Esri products as a GEOJSON.
#The method gives the file a geometry point of 0, 0 and will populate on SIGINT Island. :) 
def AddFakeGeo_Hosp(jfile):
    # create an empty dictionary of features
    jData = { "features" : None }
    jData["features"] = jfile

    #print(jData)

    # start the geojson file
    geo = {
        "type" : "FeatureCollection",
        "crs" : { "type" : "name",
                  "properties" : {
                      "name" : "EPSG:4326"
                      }
                  },
        "features" : None
        }

    feature_list = []

    # loop through json
    fCount = 0
    for feature in jData['features']:
        fCount +=1
        #This will allow the process to proceed even if one field is missing.
        #If region is missing, there will be a new "Region" called NA. Check for this in the data
        #If the Deaths figure or the date are missing, it won't be useful for our data, so the loop will pass over that iteration
        if not "PROVINCE" in feature:
            feature["PROVINCE"] = "NA"
        if not "REGION" in feature:
            feature["REGION"] = "NA"
        if not "DATE" in feature:
            continue
        features = {
            "type" : "Feature",
            "id" : fCount,
            #Fake Geometry
            "geometry" : { "type" : "Point",
                           "coordinates" : [
                               0.0, 0.0]
                           },
            #Real properties copied over
            "properties" : { 
                "DATE" : feature["DATE"],
                "PROVINCE" : feature["PROVINCE"],
                "REGION" : feature["REGION"],
                "NR_REPORTING": int(feature["NR_REPORTING"]),
                "TOTAL_IN": int(feature["TOTAL_IN"]),
                "TOTAL_IN_ICU": int(feature["TOTAL_IN_ICU"]),
                "TOTAL_IN_RESP": int(feature["TOTAL_IN_RESP"]),
                "TOTAL_IN_ECMO": int(feature["TOTAL_IN_ECMO"]),
                "NEW_IN": int(feature["NEW_IN"]), 
                "NEW_OUT": int(feature["NEW_OUT"])
            }
        }
        #add features to list
        feature_list.append(features)

    #Add Feature list to GEOJSON
    geo["features"] = feature_list

    return geo

#This method is adding fake geometry to a JSON file to allow it to be read by Esri products as a GEOJSON.
#The method gives the file a geometry point of 0, 0 and will populate on SIGINT Island. :) 
def AddFakeGeo_Mort(jfile):

# create an empty dictionary of features
    jData = { "features" : None }
    jData["features"] = jfile


# start the geojson file
    geo = {
        "type" : "FeatureCollection",
        "crs" : { "type" : "name",
                  "properties" : {
                      "name" : "EPSG:4326"
                      }
                  },
        "features" : None
        }

    feature_list = []

    # loop through json
    fCount = 0
    for feature in jData['features']:
        fCount +=1
        #This will allow the process to proceed even if one field is missing.
        #If region is missing, there will be a new "Region" called NA. Check for this in the data
        #If the Deaths figure or the date are missing, it won't be useful for our data, so the loop will pass over that iteration
        if not "REGION" in feature:
            feature["REGION"] = "NA"
        if not "AGEGROUP" in feature:
            feature["AGEGROUP"] = "NA"
        if not "SEX" in feature:
            feature["SEX"] = "NA"
        if not "DEATHS" in feature: 
            continue
        if not "DATE" in feature:
            continue
        features = {
            "type" : "Feature",
            "id" : fCount,
            #Fake Geometry
            "geometry" : { "type" : "Point",
                           "coordinates" : [
                               0.0, 0.0]
                           },
            #Real properties copied over
            "properties" : { 
                        "DATE" : feature["DATE"],
                        "REGION" : (feature["REGION"]),
                        "AGEGROUP" : (feature["AGEGROUP"]),
                        "SEX" : (feature["SEX"]),
                        "DEATHS" : int(feature["DEATHS"]),
                         }
        }
        #add to output geojson features list
        feature_list.append(features)
       
    #put these features into the geojson
    geo["features"] = feature_list

    return geo

# %%
def job(r):

    #Belgium publishes different JSONs for Hospitalizations, Cases, and Deaths
    #These variables set up the URL path to each JSON file. 
    path1 = 'https://epistat.sciensano.be/Data'
    case2 = '/COVID19BE_CASES_AGESEX.json'
    hosp2 = '/COVID19BE_HOSP.json'
    mort2 = '/COVID19BE_MORT.json'

    my_path = os.path.abspath(os.path.dirname(__file__))
    #Adjust the timedelta to reach the target date
    date = datetime.datetime.today()
    date = datetime.datetime.strftime(date, '%Y%m%d')
    #Request the case JSON and run the processing method
    url = path1 + case2
    r = requests.get(url)
    geo = AddFakeGeo_Cases(r.json())
    filepath = os.path.join(my_path, '../json/Belgium/Cases/')
    filename = 'Belgium_Cases' + date + '.json'
    with open(os.path.join(filepath, filename), 'w+') as out:
        out.write(json.dumps(geo))

    #Request the Hospitalization JSON and run the processing method
    url = path1 + hosp2
    r = requests.get(url)   
    geo = AddFakeGeo_Hosp(r.json()) 
    filepath = os.path.join(my_path, '../json/Belgium/Hosp/')
    filename = 'Belgium_HOSP' + date + '.json'
    with open(os.path.join(filepath, filename), 'w+') as out:
        out.write(json.dumps(geo))

    #Request the Deaths JSON and run the processing method
    url = path1 + mort2
    r = requests.get(url)  
    geo = AddFakeGeo_Mort(r.json())  
    filepath = os.path.join(my_path, '../json/Belgium/Mort/')
    filename = 'Belgium_MORT' + date + '.json'
    with open(os.path.join(filepath, filename), 'w+') as out:
        out.write(json.dumps(geo))
