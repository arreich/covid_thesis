import json
import urllib.request
import os

def scrape(url, filename): 
    response = urllib.request.urlopen(url + '/query?where=1%3D1&outFields=*&returnGeometry=false&outSR=4326&f=json')
    data = json.loads(response.read())
    
    with open(filename, 'w+') as out:
        out.write(json.dumps(data))